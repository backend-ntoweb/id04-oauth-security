package app.nike.oauth.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import app.nike.oauth.clients.GestioneUtenteFeignClient;
import app.nike.oauth.models.Utente;

@Service
public class UserService implements UserDetailsService{
	
	@Autowired
	private GestioneUtenteFeignClient utenteClient;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		Utente utente = utenteClient.getUserByUsername(username);
		if(utente==null) {
			throw new UsernameNotFoundException("L'utente '"+username+"' non è stato trovato");
		}
		List<GrantedAuthority> authorities = utente.getRoles()
				.stream()
				.map(role -> new SimpleGrantedAuthority(role.getName()))
				.collect(Collectors.toList());
		
		return new User(utente.getUsername(), utente.getPassword(), utente.getEnable(), 
				true, true, true, authorities);
	}

}
