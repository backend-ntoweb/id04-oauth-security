package app.nike.oauth.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import app.nike.oauth.models.Utente;

@FeignClient(name = "id05-gestione-utente")
public interface GestioneUtenteFeignClient {
	
	@GetMapping("/user/{username}")
	public Utente getUserByUsername(@PathVariable String username);
}
